FROM ubuntu:18.04
LABEL maintainer="Ashish Agre<ashish29agre@gmail.com"

RUN mkdir -p /home/source
RUN mkdir -p /root/.m2/repository

WORKDIR /home/source

#RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN apt-get update -qq && apt-get install -y git curl zip unzip puppet mysql-server-5.7
RUN curl -s 'https://get.sdkman.io' | bash
RUN /bin/bash -c "source $HOME/.sdkman/bin/sdkman-init.sh; sdk version; sdk install java 8.0.302-open; sdk install maven 3.8.6"
#RUN sdk install java 8.0.302-open

ENV JAVA_HOME=/root/.sdkman/candidates/java/current/
ENV MAVEN_HOME=/root/.sdkman/candidates/maven/current/

RUN echo $JAVA_HOME
RUN echo 'maven home`$MAVEN_HOME`'
RUN mysql --version

COPY welcome.sh welcome.sh
RUN chmod +x welcome.sh

ENV PATH "$PATH:$JAVA_HOME/bin:$MAVEN_HOME/bin"

CMD ["/bin/bash", "welcome.sh"]
