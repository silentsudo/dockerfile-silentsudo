## Instruction for creating docker build
## Docker reference

## build
```shell
docker build -t mp-app .
```

## run
```shell
docker run --name mp-app -d mp-app
```


## Export
```shell
docker export mp-app > ../docker-images/mp-app.tar
  528  docker container rm mp-app 
  529  docker build -t mp-app .
  530  docker run --name mp-app -d mp-app
  531  docker container logs mp-app
  #remove unwanted docker images
  docker image prune 
```  
  
```shell
docker build -t silentsudo/java8-maven3.8.6-mysql5.7 -t silentsudo/java8-maven3.8.6-mysql5.7:1.0 .
```

//docker tag silentsudo/java8-maven3.8.6-mysql5.7:latest silentsudo/java8-maven3.8.6-mysql5.7:1.0

```shell
docker push silentsudo/java8-maven3.8.6-mysql5.7:latest
```

silentsudo/java8-maven3.8.6-mysql5.7:1.0
